+++
title = 'Unabridged CV'
date = 2025-01-11T20:35:47+02:00
draft = false
type = 'page'
ShowToc = true
+++

## Basic Info

- Full Name: Jans George Rautenbach
- FKA: George Rautenbach
- Fluent Languages: English, Afrikaans
- Basic understanding: German, Dutch
- Email: jans (at) rauten.co.za ([PGP key](https://libalex.singularity.net.za/rauten/jans@rauten.co.za.publickey.asc))
- Matrix: [@jans:rauten.co.za](https://matrix.to/#/@jans:rauten.co.za)
- Current City of Residence: Eindhoven, Netherlands
- Current Status: Working full-time


## Education
### University of Pretoria
> Pretoria | 2021

**BSc (Hons) Computer Science; with Distinction**. Degree included elective philosophy modules

Research project: Detecting insider threats using an accepted ontology.


### University of Cape Town
> Cape Town | 2018 - 2020

**BSc Computer Science and Philosophy; with Distinction**

#### Internal Awards

- **2019**
  - Certificate of Merit for Second Year Computer Science
  - Top 15% in the Faculty of Science.
- **2018**
  - Certificate of Merit for First Year Computer Science
  - Faculty of Science Scholarship for Second Year
  - Nominated for the Entelect Social Responsiveness Reward
  - Top 15% in the Faculty of Science.
  - Faculty of Science Dean's Merit List

#### Tutoring Experience

- **2019**: Tutor for first-year Computer Science courses (CSC1015F and CSC1016S). Includes in-person group tutoring, assignment marking, test invigilation, and test marking.

#### Involvement

- **2020**
  - [Open Mind Society](https://web.archive.org/web/20211009211730/https://openmindsociety.org.za/) founder &amp; chairperson
  - Developer Student Club tech mentor
  - Philosophy Society regular
  - Developer Society regular
  - Ballroom Dancing Society regular
- **2019**
  - Developer Society Subcom Member
    - Society website development team lead
  - Philosophy Society regular
  - Philosophy course Class Representative (PHI2041S: Great Philosophers)
  - Ikeys Vibe member
- **2018**
  - Developer Society regular


### University of Tübingen
> Tübingen, Baden-Württemberg, Germany | 2020 January (1 mo)


- Short student exchange: Tübingen-South Africa Programme
- Academic enrichment focussed on political and moral philosophy.
- Exposure to German history, art, music, economics, politics, and other cultural aspects.
- Course on intercultural communication.
- A1-level German language course



### Jan van Riebeeck High School
> Cape Town | 2013 - 2017 (5 years)

#### Grade 12: Student Council Member &amp; Head of the Tech Portfolio

Each matric student council member chose or was assigned a portfolio (a subdivision of the school) for them to manage. Furthermore, each portfolio had the chance to seek members to help run it. The more members the quicker things get done, but the more difficult it becomes to manage people. Portfolio heads are left to make all operational decisions on their own.

My first choice was the Tech Portfolio because, with my combined knowledge and experience, technology was my strongest suit. The portfolio is responsible for providing, setting up and managing the necessary sound and lighting reinforcement for school events. This covers almost literally every event the school hosts (swimming galas, athletics days, interschool rugby matches, plays and cultural performances) as well as a lot of day-to-day operations (weekly hall gatherings and other announcements). The events covered could take up to 2 hours and 6 trained students to set up. Although this was a very specialised task requiring a lot of effort, the real difficulty came in with the live managing of the events' tech. Live tech management required high-level thinking and quick decision making under stressful circumstances. There was only so much the regular techies could do. When something went wrong during an event, it was entirely up to the live tech managers to devise and implement a solution as quickly as possible. There is nothing an audience hates more than "technical issues". Because of the large amounts of stress caused, there were very few students that were up to the task of live tech management.

#### Grade 11: Co-Head of Matric Farewell Committee

The matric farewell committee was a group of 15 learners, elected by their peers basically to organise the matric farewell (the grade above; not our own). In reality, the committee did much more than just the organisation of the huge event. It was also responsible for collecting all the funds necessary (~R150 000). This meant that the committee was in charge of every single for-profit event of the school.

Except for participating in general committee discussion and voting, I was also in charge of all tech-related decisions as well as oversight and execution of sound and lighting management at all Committee-ran events.

#### Tutoring

Since Grade 9 I have been tutoring various subjects that I excel at, including Mathematics, IT, Business Studies and Accounting, to learners in my grade and lower. The classes are a couple of times per term and I have been told that they really help learners achieve better results in exams.

#### Internal Awards

- Grade 12
  - Top in Business Studies
  - Top in IT
  - Academic Colours (80% Avg)
  - 5th Overall Grade Position
- Grade 11
  - Top in Accounting
  - Top in Business Studies
  - Top in IT
  - Top in Mathematics
  - Full Academic Colours (80% Avg)
  - 6th Overall Grade Position
- Grade 10
  - Top in Business Studies
  - Full Academic Colours (85% Avg)
  - 5th Overall Grade Position


## Work Experience

### Rabobank (via Tech Seals)

#### Senior DevOps Engineer

> Utrecht, NL | Nov 2024 - present

- Lead an interdepartmental project to centralise all of Rabobank's authorisation into a realtime access service. This significantly simplified the bank's architecture and enabled powerful policy controls for thousands of applications.
- Was in charge of integrating with multiple different business units and architecting solutions that balance between strengthening the realtime access core product and serving application-specific needs.

### Afrikaburn

#### Fullstack Engineer

> Cape Town | Aug - Nov 2024

- Implemented a series of backend workflows, frontend pages, and interoperability integrations for Afrikaburn's management platform, The Information Machine. Mainly PHP coding and some DevOps. 

### Electrum Payments

#### Platforms Engineer

> Cape Town | Sept 2023 - Aug 2024 (1 year)

- Lead an infrastructure project to upgrade 50+ AWS databases from version 5 to version 8 with next to no downtime. The upgrade saved Electrum thousands of dollars in AWS extended support fees, and significantly improved transaction throughput and RDS stability. The project included building automation tools which can be used to automate future database upgrades.
- Architected, integrated, and maintained the infrastructure for various high-throughput Kafka data pipelines for purposes of reconciliation, monitoring, and business intelligence. Engineered the automated provisioning of Elasticsearch dashboards and alerts for site reliability.
- Mentored and trained multiple new-starters in the Platforms team, leading them to produce Electrum-standard high-quality software in a matter of months.
- Started and ran a department-wide engineering initiative for gathering feedback about and improving the Electrum Internal Developer Platform (IDP). The initiative was crucial in prioritising and architecting the Platform Team's work.
- Met regularly with the CTO and Platforms Lead to prioritise and architect work on the IDP in accordance with ongoing and planned customer projects as well as developer feedback.
- Consistently upheld high standards wrt platform quality, usability, user acceptance testing, documentation, and actioning developer feedback. Our IDP allowed for quick and easy onboarding of new customers and services, with "batteries included" features such as self-healing clusters, automated backups, zero-downtime deployments, highly available applications, networking, cronjobs, Kafka events, and databases. Our CI/CD processes allowed devs to deploy their code changes with minimal effort and high trust due to extensive automated testing, patching, and zero-downtime deployments.

#### Intermediate Software Engineer

> Cape Town | Feb - Sept 2023 (7 months)

**Contributions**:

- Writing, testing, and deploying spec'd features, and owning said features throughout the entire SDLC.
- Analysing complex feature solutions in a multi-million SLOC codebase, and overseeing said features' development throughout the SDLC.
- Contributing to many internal brainstorming and solution architecture discussions to improve the Electrum code ecosystem, and ensure new solutions will be compatible, supported, and maintainable.
- Playing the role of Technical Lead for multiple different projects, which entails communicating effectively with project stakeholders (team leads, project managers, and customers) to provide accurate timelines and highlight potential risks early; doing the majority of the project's solution architecture while keeping engineering stakeholders (senior architects, and other code owners) in the loop; managing project resources and overseeing development of project code throughout the SDLC; and working together with Electrum's and customers' business analysts to construct high-level multi-party solutions.
- Supporting Electrum's infrastructure and software after hours for multiple week-long shifts per year. This includes being on call at all times after business hours; staying on top of alerts and ensuring all systems are functioning nominally; engaging with customers' and third parties' after hours teams to resolve technical issues ASAP after they occur; communicating downtime timeously to customer success staff.
- Hosting regular internal Engineering meetings with the purpose of disseminating useful information across the entire department. Topics include Electrum-specific tools, AWS, Docker, Kubernetes, and computer and business ethics. 
- Organising team social events to improve team morale.
- Mentoring of interns and new hires to ensure a smooth and quick onboarding experience, and also provide effective teach-a-man-to-fish style learning.
- Organising and participating in university and tech community outreach initiatives. This includes speaking at tech talks hosted by Electrum on various university campuses in the Western Cape, and also helping out with off-site university recruitment events.
- Lead a 3rd-level engineering support team with the purpose of resolving tickets raised by customers and internal stakeholders. As ticket leader, I did deep-dive investigations into Electrum payment systems and infrastructure, as well as coordinated with customer technical staff in order to effectively resolve production issues from an engineering perspective. Note that this role is rotational.
- Organised and ran multiple engineering outreach initiatives, including hosting tech talks for technologists in the Cape Town area together with the local [Google Dev Group](https://gdg.community.dev/gdg-cape-town/) Cape Town chapter. On many of these occasions I was not only involved with organisation, but also delivered presentations as an event speaker.

**Internal programs**:

- Electrum Personal Leadership Programme: Run by [Leadership Insight South Africa](https://leadershipinsight.co.za/), this multi-month program assists a select grouping of Electrum employees in developing their leadership skills both in and outside the workplace.

#### Junior Software Engineer

> Cape Town | Jan 2022 - Feb 2023 (1 year 1 month)

Responsibilities include:

- Coding up spec'd Electrum services and plugins
- Contributing to Electrum's developer processes
- Developing interpersonal relationships (yup, that's in the contract)
- Analysing the Electrum ecosystem and authoring solution specifications
- Tech leading multiple customer projects


### Displode
> Remote | Sept 2021 - Jan 2022 (5 months)

Position: **Fullstack Developer**

Wrote a substantial portion of the UI code, backend storage schema, and interoperation programming for Displode's flagship platform, [Sundial](https://www.displode.com/sundial). Sundial is an orienteering sport management tool used to log the race times of orienteers. The system also includes hardware components, namely energy efficient tag points capable of operating under extreme weather conditions and transmitting data to tags to be captured centrally by the software client running on laptops positioned at the finish line.


### Centre for AI Research
> Remote | Dec 2020 - Jan 2021 (2 months)

Position: **Research Intern**

Made major contributions in a small research unit that developed a propositional logic parsing platform ([link](https://github.com/koellewe/logic-app)). The platform was deployed for testing by professors and students in departments Computer Science, Mathematics, and Philosophy at the University of Cape Town. The platform formed the basis for multiple research projects into Knowledge Representation and Reasoning in 2021.


### HyperionDev
> Remote | May 2020 - Jul 2021

Position: **Part-time technical consultant**

During my time at HyperionDev I have had many dynamic responsibilities, including:

- **Technical content author**: writing and reviewing technical guides and tasks on an array of different technical subjects, including version control, containerisation, web development, database architecture, and software engineering best practices.
- **Code reviewer**: reviewed student-submitted solutions to technical tasks on various computer science topics.
- **Lecturer**: creating lecture content and presenting online lectures for HyperionDev students on various computer science topics.
- **Tech speaker**: created and delivered presentations on various cutting-edge tech topics to HyperionDev mentors, lecturers, and general staff.
- **SAQA content aligner**: aligned various guides and tasks to comply with SAQA standards.


### Electrum Payments (Intern)
> Cape Town | June - July 2019 (4 weeks)

Position: **Software Engineering Intern**

Responsibilities:

- Creating an API in a remarkably big and sensitive project space.
- Porting the feature for a legacy system.
- Writing documentation for both implementations.
- Managing the progression of my features through the full devops integration lifecycle. (Repo - CI - Static Analysis - RC - Dev testing env - Prod)
- Daily reporting at standups as well as project presentation.


### ACI Worldwide
> Cape Town | November 2018 - January 2019 (3 months)

Position: **Software Engineering Intern**

ACI Worldwide is a massive 40+ year old tech corporation specialising in the payments industry and has a well-established international brand. I was hired as an intern at the Cape Town office for the Summer holidays. Working at ACI was my first in-depth exposure to serious big corporate culture. Bureaucracy, code reviews, programming standards, sprint standups and project managers to report everything to. I was thrown into an old, distributed multi-million LoC project with multiple languages as components, told to figure out how it works and given a list of enhancements to implement. The other employees were helpful, but more than most of the work was mine to bear. None the less, after I was settled in properly, I starting closing tickets at a rapid pace. So much so, I completed my project well before the PM's ETR. I was asked to join another team and assist them in their tasks until the end of my contract.

The enhancements I did on my originally assigned project have since been released into production and is now running on ACI's clients' servers all around the world.

**Technologies used**: Java, Delphi, Python, Jenkins, Perforce, Angular, Django, SQL Server.


### Connect @ 49
> Cape Town | October 2018 - December 2018 (3 months)

Position: **Private tutor**

I tutored at an upmarket private tutoring agency in the Parklands area. The students here have parents with very high standards. The pressure on the local tutors was immense. My tutees did, however, see a solid increase in his marks for the year-end exam.


### MWR InfoSecurity
> Johannesburg | July 2018 (2 weeks)

Position: **Software Engineering Intern**

MWR InfoSecurity is an international tech company that specialises in information security. The business is split into two distinct wings: consultancy and development. The former is tasked with attempting to break into a client's systems and write a report on what features of their system are unsafe. The latter is the creation of the solution to these security threats. I was flown up to their South African headquarters to intern at the development department. The time I spent there got me some great first-hand experience of what it's like to work in the information security industry.

The project I was tasked with did not directly involve working on MWR's systems, however, it did focus a lot on technologies used in the industry. Because of the non-disclosure agreement they had me sign, I cannot disclose what exactly my project entailed. I can, however, say that I learned much more than I was expecting to and my time there feels like the equivalent of passing a course.

**Technologies used**: NoSQL, ElasticSearch, Kibana, Python, Sentiment Analysis, Linux Server


### ABSA Aliens
> Cape Town | Jun 2018 (2 weeks)

Position: **Software Engineering Mentee**
I interned at ABSA's tech innovation wing, called Aliens. The interns were split into teams and each assigned the task to come up with an idea and implement it within the two weeks. My team worked on a service called Clubeau. The service had a RESTful API, web client and Android client, all with different use cases. Implementing the idea was a challenging piece of work, but in the end, we finished a fully functioning proof of concept.

During the internship, the team did morningly standups and extensively used git and Trello to manage Clubeau's progress. Every member was to some degree involved in every aspect of Clubeau, but I was for the most part responsible for the API.

**Technologies used**: Android, Java, PHP, RESTful API, Linux Server, Agile Development


### SkillUp Tutors
> Cape Town | Mar 2018 - December 2018 (10 months)

Position: **Private tutor**

I regularly tutored via a service called SkillUp. Tutors are allowed to register their services (after a vetting process and interview for determining competence by a SkillUp representative). Students can then browse through tutors and pick the one they feel most comfortable with. I very quickly gained quite a few students and filled up my timetable.

I mostly tutored Matric Mathematics and university-level Computer Science.


### UberEats
> Cape Town | Oct 2017 - Feb 2018 (5 months)

Position: **Independent driver**

During my Matric December holiday, I worked part-time delivering food through UberEats. The job entailed driving to various restaurants in Cape Town, making a pickup and delivering to customers. The job had quite some pressure as both the restaurant and customer could live-track me while driving. No slacking allowed.


### Other

- **DJ's Assistant at DJ Dean SA** (Sept 2016) - During the Jan van Riebeeck High's Matric Farewell in The Bay Hotel on the 29th September 2016, I was assistant to the private DJ, Dean Hewitt. This involved help with setup of sound and lighting equipment as well as live management of the dance performers' lighting. The event in its entirety was a huge success and claimed to be the best in years by many.

- **Job Shadowing at Old Mutual South Africa** (Jun 2016) - A very insightful day spent at OMSA's headquarters. I met with many of the staff and learned a lot about what exactly goes on in Old Mutual and how the employees work together to get jobs done. In my interviews, I was taught tonnes of information about how exactly every individual employee does what they do and thereby contributes to Old Mutual's success. It was more than interesting to learn about these people's lives and I was glad to have made some connections in the process.

- **Bartender for UCT Yacht Club** (Mar 2016) - A friend and I got a job to tend a bar at a medium-sized UCT student-organised party. It was a lot of work, moving alcohol, mixing brews and handling cash for over 100 students, but the tips, as well as the experience, was well worth it.



## Skills
### Technical

- Java &amp; OOP
  - Custom de/serialisation
  - Reflection
  - Parallelisation and concurrency
  - JSVC
  - Maven
  - JOOQ
- Linux
  - System Administration
  - High performance computing cluster setup and management
- Cloud Native
  - Kubernetes (helm, argo, eks)
  - Docker containerisation
  - Disaster recovery
  - Monitoring &amp; alerting: prometheus, grafana, ELK stack
- Python
  - MPI
  - pip
  - pytest
  - concurrency
- SQL &amp; database architecture
  - MySQL, SQL Server, SQLite, PostgreSQL
  - AWS JDBC wrapper, password rotation handling
  - Flyway, database migrations
  - Blue-Green deployments
- RESTful APIs
- DevOps
  - GitHub Actions
  - GitLab CI/CD
  - Artifactory/Jfrog
  - Nexus
  - CircleCI
  - Azure DevOps
- DSL programming
  - xtext
  - Eclipse Modelling Framework (EMF)
- Android Development
- PHP
- Kotlin
- Go
- Web Development
  - Angular CLI
  - ReactJS
  - Redux
  - VueJS
  - Blazor Webassembly
  - Electron
- XML, XSD and conceptual modelling
- NoSQL (ElasticSearch, MongoDB)
- git
- Cloud IaaS Providers
  - AWS
  - Azure
  - Hetzner
  - Backblaze B2
  - Oracle Cloud
- SSL Certificate Management


#### Some experience

- Digital Forensics
- Delphi
- Perforce
- Django
- ELK stack
- Artificial Moral Agents (AMA's)
- Sentiment Analysis
- Wearable Development
- Ontology engineering (OWL/RDF/XML)
- Computer/CPU architecture
- Ruby
- R      
- C#
- C


### Soft
#### Standard

- Clear communication
- Teamwork, leadership and tactful engagement
- Problem identification and initiative to solve it
- Adaptability to new methodologies and approaches when necessary
- Agile development

#### Philosophy in the workplace

- Comprehensive ethical knowledgebase and reasoning ability
- Concise explanatory ability
- Intelligible and coherent argumentation
- Rigorous comprehension of complex ideas and systems
- Scepticism and curiosity
- Independent thinking


## Other company interest
### Offers received and rejected
The following are offers I received from companies and subsequently rejected for either logistical or personal moral apprehensions.

- Jumo.world - Software Engineer
- Yellow.africa - Software Engineer
- [Reisiger](https://www.reisiger.io/) - Software Engineer
- Amazon Web Services - internship
- Skynamo (Subsidiary of Alphawave) - internship
- SOLIDitech - internship
- Entelect - internship
- Digsconnect - software engineering position
- Matchbox - software engineering position
- Golden Key Honours Society - membership (2018 and 2019)
- UCT Google Developer Student Club - club lead position
- BDO - technology graduate program
- Dado Agency - Web Developer position
- Telic Consulting - Security architecture
- Nedbank - Digital forensics team


### Interviews attended
The following are opportunities I interviewed for in person, but was ultimately not accepted for.

- Allan Gray Orbis - scholarship
- Boston Consulting Group - internship
- UCT Science Student Council 2020 - academic chairperson
- cloudandthings.io - SE
- Maskew Miller Learning - SE
- Naked Insurance - Fullstack Dev


## Certifications

### Technical

- **[Azure Fundamentals (AZ-900)](https://learn.microsoft.com/en-us/users/jansrautenbach-9514/credentials/c2ab0c64dad9551)**

### Musical

- **Rock School Electric Guitar Grade 1**
- **UNISA Theory of Music Grade 4**


## External Awards
### Programming Competitions

- **BDO Remote Hackathon 2020**: 2nd place in first round
- **Google Hash Code 2020**: 3rd place in UCT
- **CHPC Student Cluster Competition 2019**: 3rd place in South Africa.
- **ICPC 2019**
  - 3rd place in the Cape Town region
  - 5th place in the South Africa region
  - 7th place in the Africa region
- **Standard Bank Technology Impact Challenge 2019**: Results not released.
- **Entelect University Cup 2019**: Placed 25th out of 73 teams
- **Google Hash Code 2019**
  - Placed 51st in South Africa out of about 100 teams.
  - Placed 3800th worldwide out of about 7000 teams.
- **ICPC International Collegiate Programming Contest 2018**: Achieved: Best UCT team of first-time entrants
- **Entelect University Cup 2018**: Achieved: 11th Position out of 61 Teams
- **IITPSA Computer Talent Search 2016**: Achieved: Gold Certificate


### Other Awards

- **Dado Independent Project Competition 2020**: [First place](https://www.notion.so/Dado-Demos-a4bb1e12d6d246efabe843080c5059f1)
- **UCT SSC Science Got Talent 2020**: First place
- **Electrum Bursary**: Achieved a full tuition bursary for 3rd year and honours.
- **Die Burger Wiskunde-Kompetisie 2017**: Achieved: Top 50 Nationally
- **ATKV Tienertoneel 2017**
  - Production placed 4th Nationally
  - Gold Certificate for Lighting Management
  - Gold Certificate for Lighting Design
  - Nomination for Best Handling of Technical Aspects
- **Mentorvennoot NPC Snapspel 2016**: Achieved: Champion Entrepreneur School


## Media Exposure

- **Blog of Jonathan Carter** (Aug 2023): Participated at [Debian's 30th birtday party](https://jonathancarter.org/2023/08/17/debian-30th-birthday-local-group-event-and-interview/) hosted by DebianZA
- **@GDGCapeTown** (June 2023): Presented on AI Ethics at [a Google Dev Group Cape Town event](https://web.archive.org/web/20230523215110/https://gdg.community.dev/events/details/google-gdg-cape-town-presents-containerisation-and-scaling-software/) co-hosted by Electrum ([video](https://www.youtube.com/watch?v=8lExiBALozo))
- **The Closing Bracket** (Jan 2022): Authored [an article on digital personhood](https://issuu.com/closingbracket/docs/the_closing_bracket_issue_2) (p. 12)
- **RobOntics 2021** (Sept 2021): [Presented](https://robontics2021.github.io/programme/) my research into Artificial Moral Agents.
- **Noteworthy** (Apr 2021): Authored [an article on privacy ethics](https://blog.usejournal.com/reclaiming-your-soul-digitally-5c2449150b15)
- **The Closing Bracket** (Jan 2021): Authored [an article on computer ethics](https://issuu.com/closingbracket/docs/the_closing_bracket_issue__1). (p. 8)
- **UCT Science Faculty Newsletter** (Dec 2020): [Article about my undergrad research](http://www.science.uct.ac.za/sites/default/files/image_tool/images/26/news/Science%20Matters%20December%202020%20final.pdf) (p. 15).
- **CoGrammar** (Sept 2020): Featured as an interviewee on the [career day seminar](https://youtu.be/1Ob0JH6Aer0).
- **UCT News** (May 2020): [Article about a game I made in lockdown](https://www.news.uct.ac.za/features/lockdownletters/-article/2020-05-06-bringing-laughs-to-those-in-lockdown).
- **UCT Developer Society Blog** (Mar 2020): Authored a [technical guide on advanced custom sorting and pagination](https://medium.com/@uctdevsoc/how-to-sort-posts-like-a-pro-developing-a-post-comment-platform-953b88466a50).
- **University of Tübingen International Office** (Jan 2020): [Exchange program philosophy report](https://uni-tuebingen.de/en/international/sprachen-lernen/deutschkurse/kurzzeit-programme/suedafrika-programm/programm-2020/tuebingen-diary/philosopy/)
- **UCT News** (Nov 2019): [Interview about CHPC Student Cluster Comp](https://www.news.uct.ac.za/article/-2019-11-26-students-head-to-national-computing-competition-final).
- **Oudtshoorn Courant** (Oct 2019): [Small mention in local news](https://www.oudtshoorncourant.com/News/Article/General/21-1km-op-haar-21ste-201910210148).
- **UCT CS Dept Newsletter** (Oct 2018): [Team mentioned as achievers in ICPC Regionals.](https://www.cs.uct.ac.za/uct2019s-team-201cdysfunctional-programmers201d-wins-2018-icpc-southern-africa-regional-programming-contest)
- **Netwerk24** (Aug 2018): Authored an [article on depression](https://www.netwerk24.com/Stemme/Die-Student/depressie-jy-hoef-nie-alleen-die-drag-te-dra-nie-20180823)
- **UCT Film Society - Shotties** (Apr 2018): Sound Lead in short film ["Extra Milk"](https://www.youtube.com/watch?v=nBbJdAHu6bE&feature=youtu.be)
- **City Varsity Student Film Screenings** (Oct 2017): Extra in "Girl with Kaleidoscope Eyes"
- **Die Burger** (Sept 2017): [Mention as Top 50 in Maths Competition](https://www.facebook.com/CumLaudeMedia/posts/1861694520514367)


## Publications
- Modelling Ethical Theories for Artificial Moral Agents (2019)
  - Supervised by Prof Maria Keet ([blog post](https://keet.wordpress.com/2020/03/05/  digital-assistants-and-amas-with-configurable-ethical-theories/))
  - Affiliated with UCT CS Dept
  - Paper accepted by UCT.
  - Paper accepted for presentation by AI &amp; Ethics in Society conference (New York City, 2020). Unable to attend for logistical reasons.
  - Publicised on arXiv (by Cornell University) - [link](https://arxiv.org/abs/2003.00935)
  - Published in The Join Ontology Workshops (Vol 2708, Italy, Aug 2020) - [link](http://ceur-ws.org/Vol-2708/)
  - Presented at [Robontics 2021](https://robontics2021.github.io/programme/) (Bolzano, Italy (virtual))


## Personal
### Hobbies

Code isn't all I write. I also run a [blog](https://stelsels.rauten.co.za) where I discuss philosophical concerns, often relating to technology.

Outside of tech I'm a huge electronic music afficionado and festival fanatic. I also do some amateur DJing - listen to my mixes [here](https://videos.rauten.co.za/c/revibalize/videos).

### Open Source
Most of my [personal projects](https://waypoint.rauten.co.za/projects) are open source.
Beyond these, you can follow my open source contributions on my [personal kanban board](https://waypoint.rauten.co.za/jans-oss-kanban).


### Expos, Indabas, Workshops and other learning experiences

- Ethics Champion Course [(The Ethics Institute of South Africa)](https://tei.org.za) 2022 - participant
- UNited hackathon (UCT DevSoc) 2021 - guest solution judge
- UCT Futures in Health Accelerator Project design workshop series, 2019
- #BreakTheRules Summer 2019
- McKinsie &amp; Company Digital: Student workshop 2019 - software design, methodology and presentation
- Facebook mentorship program 2019 - problem-solving and soft skills required for the software engineering industry
- #BreakTheRules Winter 2019 - UCT Software internship expo
- UCT Computer Science tutor and mentor training 2019
- [Deep Learning IndabaX Western Cape 2018](https://indabax.github.io/)
- #BreakTheRules Summer 2018 
- #BreakTheRules Winter 2018
