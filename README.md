# Rauten website

Source code for [rauten.co.za](https://rauten.co.za). Runs as a [Hugo](https://gohugo.io) site.

## Development

```sh
git submodule update --init --recursive
hugo server --buildDrafts
```

Create a new page:

```sh
hugo new my-cool-page.md
```

## Deployment

```sh
hugo build -d dist
```
